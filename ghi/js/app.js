// function createPlaceHolder() {
//   return `
//     <div class="card">
//       <img src="..." class="card-img-top" alt="...">
//       <div class="card-body">
//         <h5 class="card-title placeholder-glow">
//           <span class="placeholder col-6"></span>
//         </h5>
//         <p class="card-text placeholder-glow">
//           <span class="placeholder col-7"></span>
//           <span class="placeholder col-4"></span>
//           <span class="placeholder col-4"></span>
//           <span class="placeholder col-6"></span>
//           <span class="placeholder col-8"></span>
//         </p>
//         <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
//       </div>
//     </div>
//   `;
// }

function createCard(name, description, pictureUrl,starts,ends,location) {
  return `
      <div class="col-6 col-md-4">
          <div class="shadow-lg mb-3 bg-body-tertiary rounded">
              <img src=${pictureUrl} class="card-img-top">

              <div class="card-body">
                  <h5 class="card-title">${name}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                  <p class="card-text">${description}</p>
              </div>
              <div class="card-footer">
                  <p>${starts}-${ends}</p>
              </div>

              <p class="card-text placeholder-glow">
          </div>
      </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();


      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start = new Date(details.conference.starts).toLocaleString()
          const ends = new Date(details.conference.ends).toLocaleString()
          const location = details.conference.location.name
          const html = createCard(name, description, pictureUrl,start,ends,location);

          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
      return `
      <div class="alert alert-info" role="alert">
          A simple info alert—check it out!
      </div>
      `
      console.error(e);
      // Figure out what to do if an error is raised
    }

});
